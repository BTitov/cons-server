#cons-server

#Setup

### MySql
1. Download MySql.
2. Create db `cons_db`. 

### Config
Create .env.local file in environments folder and write it like .env.template file.

# Scripts

### First run
1. Check if npm dependencies are installed (use `npm install` if not).
2. Make sure if MySql feels fine.
3. Run `npm run rebuild` to compile project and copy assets to dist.
4. Run `npm run start:local` to start app local.
