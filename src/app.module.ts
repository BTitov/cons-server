import {Module} from '@nestjs/common';
import {CardModule} from './card/card.module';
import {ItemModule} from './item/item.module';
import {AuthModule} from './auth/auth.module';
import {DbModule} from './db/db.module';
import {ConfigModule} from './config/config.module';
import { UserModule } from './user/user.module';
import { PayingModule } from './paying/paying.module';

@Module({
  imports: [
    CardModule,
    ItemModule,
    AuthModule,
    DbModule,
    ConfigModule,
    UserModule,
    PayingModule
  ],
  controllers: [],
  providers: []
})
export class AppModule {
}
