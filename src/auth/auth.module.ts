import {Module} from '@nestjs/common';
import {AuthController} from './auth.controller';
import {AuthService} from './auth.service';
import {UserModule} from '../user/user.module';
import {PassportModule} from '@nestjs/passport';
import {AuthJwtStrategy} from './auth-jwt.strategy';
import {ConfigModule} from '../config/config.module';

@Module({
  imports: [
    PassportModule.register({defaultStrategy: 'jwt'}),
    UserModule,
    ConfigModule
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    AuthJwtStrategy
  ]
})
export class AuthModule {
}
