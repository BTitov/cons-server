import {Inject, Injectable} from '@nestjs/common';
import {DiConstants} from '../di/constants';
import {UserRepository} from '../user/user.repository';
import {UserEntity} from '../user/user.entity';
import * as Bluebird from 'bluebird';
import * as bcrypt from 'bcrypt';
import {throwBadRequestErrorIf, throwInternalServerError, throwUnauthorizedError, throwUnauthorizedErrorIf} from '../utils/error/error.utils';
import {ConfigService} from '../config/config.service';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthService {
  constructor(@Inject(DiConstants.UserRepository) private readonly userRepository: UserRepository,
              private readonly configService: ConfigService) {
  }

  public login(user: UserEntity): Bluebird<{token: string}> {
    return Bluebird.resolve(this.userRepository.findOne({email: user.email}))
      .tap((found: UserEntity) => throwBadRequestErrorIf(!found, `Provided wrong email`))
      .tap((found: UserEntity) =>
        bcrypt.compare(user.password, found.password)
          .then(result => throwBadRequestErrorIf(!result, `Provided wrong password`))
      )
      .then((found: UserEntity) => ({token: this.createAccessToken(found.id)}))
      .catch((error: any) => throwInternalServerError(`Can't login user with email '${user.email}'`, error));
  }

  public register(user: UserEntity): Bluebird<UserEntity> {
    return Bluebird.resolve(this.userRepository.findOne({email: user.email}))
      .then((found: UserEntity) => throwBadRequestErrorIf(!!found, `User with such email already exists`))
      .then(() => this.encodePassword(user.password))
      .then((pass: string) => {
        user.password = pass;
        return this.userRepository.save(user);
      })
      .catch((error: any) => throwInternalServerError(`Can't register user with email '${user.email}'`, error));
  }

  public validateUser(id: number): Bluebird<UserEntity> {
    return Bluebird.resolve(this.userRepository.findOne(id))
      .tap((found: UserEntity) => throwUnauthorizedErrorIf(!found, `Unauthorized`))
      .catch(() => throwUnauthorizedError(`Unauthorized`));
  }

  private createAccessToken(id: number): string {
    return jwt.sign({id}, this.configService.config.ACCESS_TOKEN_SECRET, {expiresIn: this.configService.config.ACCESS_TOKEN_EXPIRES_IN});
  }

  private encodePassword(password: string): Bluebird<string> {
    return Bluebird.resolve(bcrypt.genSalt(10))
      .then((salt: string) => bcrypt.hash(password, salt))
      .catch((error: any) => throwInternalServerError(`Can't encode password`, error));
  }
}
