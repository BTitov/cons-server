import {Injectable} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import * as Bluebird from 'bluebird';
import {ExtractJwt, Strategy} from 'passport-jwt';
import {AuthService} from './auth.service';
import {ConfigService} from '../config/config.service';
import {throwUnauthorizedErrorIf} from '../utils/error/error.utils';
import {UserEntity} from '../user/user.entity';

@Injectable()
export class AuthJwtStrategy extends PassportStrategy(Strategy) {

  constructor(private readonly authService: AuthService,
              private readonly configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromHeader('x-access-token'),
      secretOrKey: configService.config.ACCESS_TOKEN_SECRET
    });
  }

  public validate(payload: any): Bluebird<UserEntity> {
    const rawId: string = payload.id;
    const id = parseInt(rawId, 10);
    throwUnauthorizedErrorIf(!id, 'Unauthorized');
    return this.authService.validateUser(id);
  }
}
