import * as Joi from 'joi';

export const loginSchema: Joi.ObjectSchema = Joi.object({
  email: Joi.string().email({minDomainAtoms: 2}).required(),
  password: Joi.string().min(6).required()
});
