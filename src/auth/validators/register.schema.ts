import * as Joi from 'joi';

export const registerSchema: Joi.ObjectSchema = Joi.object({
  name: Joi.string().regex(/^[A-Za-z ]+$/).min(3).max(100).required(),
  email: Joi.string().email({minDomainAtoms: 2}).required(),
  password: Joi.string().min(6).required()
});
