import {Body, Controller, Post} from '@nestjs/common';
import * as Bluebird from 'bluebird';
import {UserEntity} from '../user/user.entity';
import {AuthService} from './auth.service';
import {validate} from '../utils/api.utils';
import {registerSchema} from './validators/register.schema';
import {loginSchema} from './validators/login.schema';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Post('login')
  public login(@Body() rawUser: UserEntity): Bluebird<{token: string}> {
    const valid = validate(rawUser, loginSchema);
    const user = Object.assign(new UserEntity(), valid);
    return this.authService.login(user);
  }

  @Post('register')
  public register(@Body() rawUser: UserEntity): Bluebird<UserEntity> {
    const valid = validate(rawUser, registerSchema);
    const user = Object.assign(new UserEntity(), valid);
    return this.authService.register(user);
  }
}
