import * as helmet from 'helmet';
import * as compression from 'compression';
import * as rateLimit from 'express-rate-limit';
import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {ExceptionsFilter} from './utils/error/exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(helmet());
  app.use(compression());
  app.use(rateLimit({
    windowMs: 15 * 60 * 1000,
    max: 100
  }));
  app.useGlobalFilters(new ExceptionsFilter());
  await app.listen(3000);
}

bootstrap();
