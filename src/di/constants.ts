export enum DiConstants {
  DbConnection = 'DB_CONNECTION',
  ItemRepository = 'ItemRepository',
  UserRepository = 'UserRepository',
  CardRepository = 'CardRepository'
}
