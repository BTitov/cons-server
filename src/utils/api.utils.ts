import * as jwt from 'jsonwebtoken';
import * as _ from 'lodash';
import {throwBadRequestErrorIf, throwUnauthorizedErrorIf} from './error/error.utils';
import * as Joi from 'joi';

export function validate(obj: any, schema: Joi.SchemaLike, objName = 'data') {
  const {error, value} = Joi.validate(obj, schema);
  throwBadRequestErrorIf(!!error, `Incorrect ${objName} provided`, error);
  return value;
}

export function getUserIdFromToken(token: string): number {
  throwUnauthorizedErrorIf(!token, `Unauthorized`);

  const decoded = jwt.decode(token);
  const decToStr = JSON.stringify(decoded);
  const decToJSON = JSON.parse(decToStr);
  const tokenUserId = decToJSON.id;

  throwUnauthorizedErrorIf(!tokenUserId, `Token doesn't contain required information`);

  const userId = parseInt(tokenUserId, 10);

  throwUnauthorizedErrorIf(_.isNil(userId), `Token contain incorrect information`);

  return userId;
}
