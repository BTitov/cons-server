export class HttpError {
  constructor(public code: number, public name: string, public message: string) {
  }
}

export function throwHttpError(code: number, name: string, message: string, previousErr?: any): any {
  if (previousErr) {
    code = previousErr.code || code;
    name = previousErr.name || name;
    message = previousErr.message ? message + `: ${previousErr.message}` : message;
  }
  throw new HttpError(code, name, message);
}

export function throwBadRequestError(message: string, previousErr?: any): any {
  throwHttpError(400, 'Bad Request', message, previousErr);
}

export function throwBadRequestErrorIf(condition: boolean, message: string, previousErr?: any): any {
  if (condition) {
    throwBadRequestError(message, previousErr);
  }
}

export function throwUnauthorizedError(message: string, previousErr?: any): any {
  throwHttpError(401, 'Unauthorized', message, previousErr);
}

export function throwUnauthorizedErrorIf(condition: boolean, message: string, previousErr?: any): any {
  if (condition) {
    throwUnauthorizedError(message, previousErr);
  }
}

export function throwNotFoundError(message: string, previousErr?: any): any {
  throwHttpError(404, 'Not Found', message, previousErr);
}

export function throwNotFoundErrorIf(condition: boolean, message: string, previousErr?: any): any {
  if (condition) {
    throwNotFoundError(message, previousErr);
  }
}

export function throwInternalServerError(message: string, previousErr?: any): any {
  throwHttpError(500, 'Internal Server Error', message, previousErr);
}

export function throwInternalServerErrorIf(condition: boolean, message: string, previousErr?: any): any {
  if (condition) {
    throwInternalServerError(message, previousErr);
  }
}

export function throwForbiddenError(message: string, previousErr?: any): any {
  throwHttpError(403, 'Forbidden Error', message, previousErr);
}

export function throwForbiddenErrorIf(condition: boolean, message: string, previousErr?: any): any {
  if (condition) {
    throwForbiddenError(message, previousErr);
  }
}
