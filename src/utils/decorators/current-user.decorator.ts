import {createParamDecorator} from '@nestjs/common';
import {getUserIdFromToken} from '../api.utils';

export const CurrentUser = createParamDecorator(async (data, req) => {
  const token = req.headers['x-access-token'];
  return getUserIdFromToken(token);
});
