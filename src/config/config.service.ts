import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from 'joi';
import {Injectable} from '@nestjs/common';

export interface EnvConfig {
  APP_PORT: number;
  DB_TYPE: string;
  DB_HOST: string;
  DB_PORT: number;
  DB_NAME: string;
  DB_USERNAME: string;
  DB_PASSWORD: string;
  ACCESS_TOKEN_SECRET: string;
  ACCESS_TOKEN_EXPIRES_IN: string;
}

@Injectable()
export class ConfigService {
  private readonly _config: EnvConfig;

  public get config(): EnvConfig {
    return this._config;
  }

  constructor(path: string) {
    this._config = this.validateInput(dotenv.parse(fs.readFileSync(path)));
  }

  private validateInput(config: object): EnvConfig {
    const envSchema: Joi.ObjectSchema = Joi.object({
      APP_PORT: Joi.number().required(),
      DB_TYPE: Joi.string().required(),
      DB_HOST: Joi.string().required(),
      DB_PORT: Joi.number().required(),
      DB_NAME: Joi.string().required(),
      DB_USERNAME: Joi.string().required(),
      DB_PASSWORD: Joi.string().required(),
      ACCESS_TOKEN_SECRET: Joi.string().required(),
      ACCESS_TOKEN_EXPIRES_IN: Joi.string().required()
    });

    const {error, value: validatedConfig} = Joi.validate(config, envSchema);
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedConfig as EnvConfig;
  }
}
