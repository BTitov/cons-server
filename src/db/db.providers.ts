import {createConnection} from 'typeorm';
import {ConfigService} from '../config/config.service';
import {DiConstants} from '../di/constants';

export const dbProvider = {
  provide: DiConstants.DbConnection,
  useFactory: (configService: ConfigService) => {
    const dropDb = process.env.NODE_ENV === 'test';
    return createConnection({
      type: configService.config.DB_TYPE as any,
      host: configService.config.DB_HOST,
      port: configService.config.DB_PORT,
      database: configService.config.DB_NAME,
      username: configService.config.DB_USERNAME,
      password: configService.config.DB_PASSWORD,
      dropSchema: dropDb,
      synchronize: true,
      entities: ['../**/*.entity{.js, .ts}']
    });
  },
  inject: [ConfigService]
};
