import {Module} from '@nestjs/common';
import {dbProvider} from './db.providers';
import {ConfigModule} from '../config/config.module';

@Module({
  imports: [ConfigModule],
  providers: [dbProvider],
  exports: [dbProvider]
})
export class DbModule {
}
