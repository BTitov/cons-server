import {AfterInsert, AfterUpdate, BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {CardEntity} from '../card/card.entity';
import {ItemEntity} from '../item/item.entity';

@Entity('user')
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({type: 'varchar', length: 100, nullable: false, unique: false})
  public name: string;

  @Column({type: 'varchar', nullable: false, unique: true})
  public email: string;

  @Column({type: 'varchar', nullable: false, unique: false})
  public password: string;

  @OneToMany(type => CardEntity, card => card.user)
  public cards: CardEntity[];

  @OneToMany(type => ItemEntity, item => item.user)
  public items: ItemEntity[];

  @AfterInsert()
  @AfterUpdate()
  public clearPassword(): void {
    delete this.password;
  }
}
