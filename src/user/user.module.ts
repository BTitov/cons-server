import {Module} from '@nestjs/common';
import {DbModule} from '../db/db.module';
import {DiConstants} from '../di/constants';
import {Connection} from 'typeorm';
import {UserRepository} from './user.repository';

const userRepoProvider = {
  provide: DiConstants.UserRepository,
  useFactory: (connection: Connection) => connection.getCustomRepository(UserRepository),
  inject: [DiConstants.DbConnection]
};

@Module({
  imports: [DbModule],
  providers: [userRepoProvider],
  exports: [userRepoProvider]
})
export class UserModule {
}
