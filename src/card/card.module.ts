import {Module} from '@nestjs/common';
import {CardController} from './card.controller';
import {CardService} from './card.service';
import {DbModule} from '../db/db.module';
import {DiConstants} from '../di/constants';
import {Connection} from 'typeorm';
import {CardRepository} from './card.repository';
import {AuthModule} from '../auth/auth.module';

export const cardRepoProvider = {
  provide: DiConstants.CardRepository,
  useFactory: (connection: Connection) => connection.getCustomRepository(CardRepository),
  inject: [DiConstants.DbConnection]
};

@Module({
  imports: [
    DbModule,
    AuthModule
  ],
  controllers: [CardController],
  providers: [
    CardService,
    cardRepoProvider
  ],
  exports: [
    cardRepoProvider
  ]
})
export class CardModule {
}
