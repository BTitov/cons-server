import {Inject, Injectable} from '@nestjs/common';
import {DiConstants} from '../di/constants';
import {CardRepository} from './card.repository';
import * as Bluebird from 'bluebird';
import {CardEntity} from './card.entity';
import * as cardValidator from 'card-validator';
import {throwBadRequestErrorIf, throwInternalServerError} from '../utils/error/error.utils';

@Injectable()
export class CardService {
  constructor(@Inject(DiConstants.CardRepository) private readonly cardRepository: CardRepository) {
  }

  public create(userId: number, card: CardEntity): Bluebird<CardEntity> {
    return Bluebird.resolve()
      .then(() => this.validateCard(card))
      .then(() => this.cardRepository.findOne({number : card.number, userId}))
      .then((found: CardEntity) => throwBadRequestErrorIf(!!found, 'Card already exists'))
      .then(() => {
        card.userId = userId;
        return this.cardRepository.save(card);
      })
      .catch((error: any) => throwInternalServerError(`Can't add card '${card.number}'`, error));
  }

  public getByUser(userId: number): Bluebird<CardEntity[]> {
    return Bluebird.resolve()
      .then(() => this.cardRepository.find({userId}))
      .catch((error: any) => throwInternalServerError(`Can't get cards by user '${userId}'`, error));
  }

  private validateCard(card: CardEntity) {
    const numberValidation = cardValidator.number(card.number);
    throwBadRequestErrorIf(!numberValidation.isPotentiallyValid, 'Card is not valid');

    const cvvValidation = cardValidator.cvv(card.cvv.toString());
    throwBadRequestErrorIf(!cvvValidation.isPotentiallyValid, 'Card is not valid');

    const expirationDateValidation = cardValidator.expirationDate(card.expirationDate);
    throwBadRequestErrorIf(!expirationDateValidation, 'Card is not valid');
  }
}
