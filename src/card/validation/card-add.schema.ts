import * as Joi from 'joi';

export const cardAddSchema: Joi.ObjectSchema = Joi.object({
  owner: Joi.string().min(3).max(100).regex(/^[A-Za-z ]+$/).required(),
  number: Joi.string().required(),
  cvv: Joi.string().required(),
  expirationDate: Joi.string().required()
});
