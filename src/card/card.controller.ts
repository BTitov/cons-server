import {Body, Controller, Get, Post, UseGuards} from '@nestjs/common';
import * as Bluebird from 'bluebird';
import {CardEntity} from './card.entity';
import {CardService} from './card.service';
import {validate} from '../utils/api.utils';
import {cardAddSchema} from './validation/card-add.schema';
import {CurrentUser} from '../utils/decorators/current-user.decorator';
import {AuthGuard} from '@nestjs/passport';

@Controller('cards')
export class CardController {
  constructor(private readonly cardService: CardService) {
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  public create(@CurrentUser() userId: number, @Body() rawCard: CardEntity): Bluebird<CardEntity> {
    const valid = validate(rawCard, cardAddSchema);
    const card = Object.assign(new CardEntity(), valid);
    return this.cardService.create(userId, card);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  public getByCurrentUser(@CurrentUser() userId: number): Bluebird<CardEntity[]> {
    return this.cardService.getByUser(userId);
  }
}
