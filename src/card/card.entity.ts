import {AfterInsert, AfterLoad, AfterUpdate, BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {UserEntity} from '../user/user.entity';

@Entity('card')
export class CardEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({type: 'varchar', unique: false, nullable: false})
  public owner: string;

  @Column({type: 'varchar', unique: false, nullable: false})
  public number: string;

  @Column({type: 'varchar', unique: false, nullable: false})
  public cvv: string;

  @Column({type: 'varchar', unique: false, nullable: false})
  public expirationDate: string;

  @Column({type: 'int', nullable: false, unique: false})
  public userId: number;

  @ManyToOne(type => UserEntity, user => user.cards)
  public user: UserEntity;

  @AfterUpdate()
  @AfterInsert()
  @AfterLoad()
  public clearPrivateInfo(): void {
    delete this.cvv;
    delete this.expirationDate;
    delete this.owner;
  }
}
