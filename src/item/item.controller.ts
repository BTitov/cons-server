import {Body, Controller, Get, Post, UseGuards} from '@nestjs/common';
import {ItemService} from './item.service';
import * as Bluebird from 'bluebird';
import {ItemEntity} from './item.entity';
import {CurrentUser} from '../utils/decorators/current-user.decorator';
import {AuthGuard} from '@nestjs/passport';

@Controller('items')
export class ItemController {
  constructor(private readonly itemService: ItemService) {
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  public create(@CurrentUser() userId: number, @Body() rawItem: ItemEntity): Bluebird<ItemEntity> {
    const item = Object.assign(new ItemEntity(), rawItem);
    return this.itemService.create(userId, item);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  public getForCurrentUser(@CurrentUser() userId: number): Bluebird<ItemEntity[]> {
    return this.itemService.getForCurrentUser(userId);
  }
}
