import {Module} from '@nestjs/common';
import {ItemController} from './item.controller';
import {ItemService} from './item.service';
import {ItemRepository} from './item.repository';
import {DbModule} from '../db/db.module';
import {Connection} from 'typeorm';
import {DiConstants} from '../di/constants';
import {AuthModule} from '../auth/auth.module';

export const itemRepoProvider = {
  provide: DiConstants.ItemRepository,
  useFactory: (connection: Connection) => connection.getCustomRepository(ItemRepository),
  inject: [DiConstants.DbConnection]
};

@Module({
  imports: [
    DbModule,
    AuthModule
  ],
  controllers: [ItemController],
  providers: [
    ItemService,
    itemRepoProvider
  ],
  exports: [
    itemRepoProvider
  ]
})
export class ItemModule {
}
