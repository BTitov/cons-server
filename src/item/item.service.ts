import {Inject, Injectable} from '@nestjs/common';
import {ItemRepository} from './item.repository';
import {ItemEntity} from './item.entity';
import * as Bluebird from 'bluebird';
import {DiConstants} from '../di/constants';
import {throwInternalServerError} from '../utils/error/error.utils';

@Injectable()
export class ItemService {
  constructor(@Inject(DiConstants.ItemRepository) private readonly itemRepository: ItemRepository) {
  }

  public create(userId: number, item: ItemEntity): Bluebird<ItemEntity> {
    return Bluebird.resolve()
      .then(() => {
        item.userId = userId;
        return this.itemRepository.save(item);
      })
      .catch((error: any) => throwInternalServerError(`Can't create item with name '${item.name}'`, error));
  }

  public getForCurrentUser(userId: number): Bluebird<ItemEntity[]> {
    return Bluebird.resolve()
      .then(() => this.itemRepository.find({userId}))
      .catch((error: any) => throwInternalServerError(`Can't find items by user '${userId}'`, error));
  }
}
