import {BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {UserEntity} from '../user/user.entity';

@Entity('item')
export class ItemEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({type: 'varchar', nullable: false, unique: false})
  public name: string;

  @Column({type: 'float', nullable: false, unique: false})
  public price: number;

  @Column({type: 'int', nullable: false})
  public userId: number;

  @ManyToOne(type => UserEntity, user => user.items)
  public user: UserEntity;
}
