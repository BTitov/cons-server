import {Inject, Injectable} from '@nestjs/common';
import * as Bluebird from 'bluebird';
import {DiConstants} from '../di/constants';
import {CardRepository} from '../card/card.repository';
import {ItemRepository} from '../item/item.repository';
import {throwForbiddenErrorIf, throwInternalServerError, throwNotFoundErrorIf} from '../utils/error/error.utils';
import {ItemEntity} from '../item/item.entity';
import {CardEntity} from '../card/card.entity';

@Injectable()
export class PayingService {

  constructor(@Inject(DiConstants.CardRepository) private readonly cardRepository: CardRepository,
              @Inject(DiConstants.ItemRepository) private readonly itemRepository: ItemRepository) {
  }

  public pay(userId: number, cardId: number): Bluebird<void> {
    return Bluebird.resolve()
      .then(() => this.itemRepository.find({userId}))
      .tap((items: ItemEntity[]) => throwForbiddenErrorIf(!items || !items.length, `You haven't any items in the basket`))
      .then(() => this.cardRepository.findOne({userId, id: cardId}))
      .tap((card: CardEntity) => throwNotFoundErrorIf(!card, `Provided card doesn't exist`))
      .delay(3000)
      .then(() => throwForbiddenErrorIf(cardId % 2 === 0, `Payment failure`))
      .tap(() => this.itemRepository.delete({userId}))
      .catch((error: any) => throwInternalServerError(`User with id '${userId}' can't pay using card '${cardId}'`, error));
  }
}
