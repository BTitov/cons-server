import {Module} from '@nestjs/common';
import {PayingController} from './paying.controller';
import {PayingService} from './paying.service';
import {CardModule} from '../card/card.module';
import {ItemModule} from '../item/item.module';
import {AuthModule} from '../auth/auth.module';

@Module({
  imports: [
    CardModule,
    ItemModule,
    AuthModule
  ],
  controllers: [PayingController],
  providers: [PayingService]
})
export class PayingModule {
}
