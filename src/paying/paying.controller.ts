import {Controller, Get, Param, UseGuards} from '@nestjs/common';
import * as Bluebird from 'bluebird';
import {PayingService} from './paying.service';
import {throwBadRequestErrorIf} from '../utils/error/error.utils';
import {CurrentUser} from '../utils/decorators/current-user.decorator';
import {AuthGuard} from '@nestjs/passport';

@Controller('paying')
export class PayingController {

  constructor(private readonly payingService: PayingService) {
  }

  @Get(':cardId')
  @UseGuards(AuthGuard('jwt'))
  public pay(@CurrentUser() userId: number, @Param('cardId') rawCardId: string): Bluebird<void> {
    const cardId = parseInt(rawCardId, 10);
    throwBadRequestErrorIf(!cardId, 'Provided bad id');
    return this.payingService.pay(userId, cardId);
  }
}
